package com.wavelabs.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class ContactDetails {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int contactId;
	private String mobileNo;
	private String emailId;
	private Address primaryAddress;
	private Address currentAddress;
	
	public ContactDetails() {}
	
	public ContactDetails(String mobileNo, String emailId, Address primaryAddress, Address currentAddress) {
		super();
		this.mobileNo = mobileNo;
		this.emailId = emailId;
		this.primaryAddress = primaryAddress;
		this.currentAddress = currentAddress;
	}

	public int getContactId() {
		return contactId;
	}

	public void setContactId(int contactId) {
		this.contactId = contactId;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public Address getPrimaryAddress() {
		return primaryAddress;
	}

	public void setPrimaryAddress(Address primaryAddress) {
		this.primaryAddress = primaryAddress;
	}

	public Address getCurrentAddress() {
		return currentAddress;
	}

	public void setCurrentAddress(Address currentAddress) {
		this.currentAddress = currentAddress;
	}

	@Override
	public String toString() {
		return "ContactDetails [contactId=" + contactId + ", mobileNo=" + mobileNo + ", emailId=" + emailId
				+ ", primaryAddress=" + primaryAddress + ", currentAddress=" + currentAddress + "]";
	}
	
}
